# TransitionEffect

**Summary**

N/A

**Remarks**

N/A

**Items**

|Name|Description|
|---|---|
|None|Specifies that no transition effect is used.|
|Stretch|Specifies that existing tiles are resized on zoom to provide a visual effect of the zoom having taken place immediately.  As the new tiles become available, they are drawn over top of the resized tiles.|

