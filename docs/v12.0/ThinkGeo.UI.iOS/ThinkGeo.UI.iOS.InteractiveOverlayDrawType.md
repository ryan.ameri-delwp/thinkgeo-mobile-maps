# InteractiveOverlayDrawType

**Summary**

N/A

**Remarks**

N/A

**Items**

|Name|Description|
|---|---|
|Default|Default value of the enumation, the same as DoNotDraw.|
|Draw|This enumeration item means that InterativeOverlay will still be drawn.|
|DoNotDraw|This enumeration item means that InterativeOverlay will still not be drawn.|

