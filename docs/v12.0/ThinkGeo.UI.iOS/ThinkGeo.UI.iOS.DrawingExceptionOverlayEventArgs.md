# DrawingExceptionOverlayEventArgs


## Inheritance Hierarchy

+ `Object`
  + `EventArgs`
    + **`DrawingExceptionOverlayEventArgs`**

## Members Summary

### Public Constructors Summary


|Name|
|---|
|[`DrawingExceptionOverlayEventArgs()`](#drawingexceptionoverlayeventargs)|
|[`DrawingExceptionOverlayEventArgs(GeoCanvas,Exception)`](#drawingexceptionoverlayeventargsgeocanvasexception)|
|[`DrawingExceptionOverlayEventArgs(GeoCanvas,Exception,Boolean)`](#drawingexceptionoverlayeventargsgeocanvasexceptionboolean)|

### Protected Constructors Summary


|Name|
|---|
|N/A|

### Public Properties Summary

|Name|Return Type|Description|
|---|---|---|
|[`Cancel`](#cancel)|`Boolean`|Gets or sets a value indicating whether this  is cancel.|
|[`Canvas`](#canvas)|[`GeoCanvas`](../ThinkGeo.Core/ThinkGeo.Core.GeoCanvas.md)|Gets or sets the canvas.|
|[`Exception`](#exception)|`Exception`|Gets or sets the exception.|

### Protected Properties Summary

|Name|Return Type|Description|
|---|---|---|
|N/A|N/A|N/A|

### Public Methods Summary


|Name|
|---|
|[`Equals(Object)`](#equalsobject)|
|[`GetHashCode()`](#gethashcode)|
|[`GetType()`](#gettype)|
|[`ToString()`](#tostring)|

### Protected Methods Summary


|Name|
|---|
|[`Finalize()`](#finalize)|
|[`MemberwiseClone()`](#memberwiseclone)|

### Public Events Summary


|Name|Event Arguments|Description|
|---|---|---|
|N/A|N/A|N/A|

## Members Detail

### Public Constructors


|Name|
|---|
|[`DrawingExceptionOverlayEventArgs()`](#drawingexceptionoverlayeventargs)|
|[`DrawingExceptionOverlayEventArgs(GeoCanvas,Exception)`](#drawingexceptionoverlayeventargsgeocanvasexception)|
|[`DrawingExceptionOverlayEventArgs(GeoCanvas,Exception,Boolean)`](#drawingexceptionoverlayeventargsgeocanvasexceptionboolean)|

### Protected Constructors


### Public Properties

#### `Cancel`

**Summary**

   *Gets or sets a value indicating whether this  is cancel.*

**Remarks**

   *N/A*

**Return Value**

`Boolean`

---
#### `Canvas`

**Summary**

   *Gets or sets the canvas.*

**Remarks**

   *N/A*

**Return Value**

[`GeoCanvas`](../ThinkGeo.Core/ThinkGeo.Core.GeoCanvas.md)

---
#### `Exception`

**Summary**

   *Gets or sets the exception.*

**Remarks**

   *N/A*

**Return Value**

`Exception`

---

### Protected Properties


### Public Methods

#### `Equals(Object)`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Boolean`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|obj|`Object`|N/A|

---
#### `GetHashCode()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Int32`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `GetType()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Type`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `ToString()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`String`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---

### Protected Methods

#### `Finalize()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Void`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `MemberwiseClone()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Object`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---

### Public Events


