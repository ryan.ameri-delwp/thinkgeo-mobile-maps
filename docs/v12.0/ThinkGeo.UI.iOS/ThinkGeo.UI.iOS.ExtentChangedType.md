# ExtentChangedType

**Summary**

N/A

**Remarks**

N/A

**Items**

|Name|Description|
|---|---|
|None|Default value for enumeration.|
|Pan|This enumeration item represents panning action for the extent interativeOverlay.|
|TrackZoomIn|This enumeration item represents track zoom in action for the extent interativeOverlay.|
|TrackZoomOut|This enumeration item represents track zoom out action for the extent interativeOverlay.|
|DoubleClickZoomIn|This enumeration item represents double-click zoom in action for the extent interativeOverlay.|
|DoubleClickZoomOut|This enumeration item represents double-click zoom out action for the extent interativeOverlay.|
|MouseWheelZoomIn|This enumeration item represents mouse wheel zoom in action for the extent interativeOverlay.|
|MouseWheelZoomOut|This enumeration item represents mouse wheel zoom out action for the extent interativeOverlay.|

